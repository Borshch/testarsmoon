<?php

$calls = $GLOBALS["result"];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Result</title>
    <link rel="stylesheet" type="text/css" href="templates/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row my-5"><b>Результаты парсинга файла</b></div>

        <?php foreach($calls['calls'] as $id => $customer): ?>
            <div class="row">Customer - <?php echo $id?></div>
            <table class="table my-5">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Дата звонка</th>
                        <th scope="col">Длительность</th>
                        <th scope="col">Номер</th>
                        <th scope="col">IP</th>
                        <th scope="col">Страна</th>
                        <th scope="col">Код континента</th>
                        <th scope="col">Совпадение континента </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $numberLine = 1; ?>
                    <?php foreach ($customer['calls'] as $call): ?>
                        <tr>
                            <th scope="row"><?php echo $numberLine ?></th>
                            <td><?php echo $call['callDate'] ?></td>
                            <td><?php echo $call['durationInSeconds'] ?></td>
                            <td><?php echo $call['dialedPhoneNumber'] ?></td>
                            <td><?php echo $call['customerIP'] ?></td>
                            <td><?php echo $call['geoByPhone']['country'] ?></td>
                            <td><?php echo $call['geoByPhone']['continent'] ?></td>
                            <td><?php echo $call['compareIpLocationWithPhoneLocation'] ? 1 : 0 ?></td>
                        </tr>
                        <?php $numberLine++ ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="row mb-0 mt-0">
                <b><?php echo 'Всего звонков - ' . $customer['statistic']['allCustomerCalls'] ?></b>
            </div>
            <div class="row mb-5 mt-0">
                <b><?php echo 'Общая длительность звонков - ' . $customer['statistic']['totalDurationCalls'] ?></b>
            </div>
            <hr>
        <?php endforeach ?>
    </div>
</body>
</html>