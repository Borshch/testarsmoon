<?php
declare(strict_types=1);


namespace App;


use App\Service\File\FileService;
use App\Service\Geo\GeoService;
use App\Service\Parse\CsvParseService;
use App\Service\Parse\PhoneParserService;

class App
{
    /**
     * First method in path for all requests
     */
    public function run()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            require_once(__DIR__ . '/../templates/index.html');
        } else {
            PhoneParserService::parsePhoneBase();
            if ($_FILES['geo']) {
                $this->getData();
            }
        }
    }

    public function getData()
    {
        if ((new FileService())->save()) {
            $customers = (new CsvParseService())->export();
            $calls = (new GeoService())->getContinent($customers);

            $parsePhone = new PhoneParserService();
            $callsWithGeoFromPhone = $parsePhone->getCountryByPhone($calls);
            $callsWithCheck = $parsePhone->compareContinentByPhoneWithContinentByIP($callsWithGeoFromPhone);
            $GLOBALS["result"] = $parsePhone->addStatisticForEachCustomer($callsWithCheck);
//            App::dd($GLOBALS["result"]);
            require_once(__DIR__ . '/../templates/result.php');
        }
    }

    /**
     * @param $value
     */
    public static function dd($value)
    {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
        die();
    }
}