<?php
declare(strict_types=1);


namespace App\Service\File;

use App\App;

class FileService
{
    /**
     * @return bool
     */
    public function save(): bool
    {
        $uploadFile = __DIR__ . '/../../../public/csv/upload.csv';

        if (move_uploaded_file($_FILES['geo']['tmp_name'], $uploadFile)) {
            return true;
        }

        return false;
    }
}