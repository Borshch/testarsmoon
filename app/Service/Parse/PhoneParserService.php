<?php
declare(strict_types=1);


namespace App\Service\Parse;


use App\App;

class PhoneParserService
{
    public static function parsePhoneBase(): void
    {
        $parsed = [];
        $data = explode(';', file_get_contents(__DIR__ . '/../../../public/csv/phones.csv'));
        foreach ($data as $country) {
            $parsed[] = explode(',', $country);
        }

        $_SESSION['country'] = $parsed;
    }

    /**
     * @param array $calls
     * @return array
     */
    public function getCountryByPhone(array $calls): array
    {
        foreach ($calls['calls'] as &$customer) {
            foreach ($customer['calls'] as &$item) {
                $item['geoByPhone'] = $this->searchCountry($item['dialedPhoneNumber']);
            }
        }

        return $calls;
    }

    /**
     * @param string $number
     * @return array|string
     */
    protected function searchCountry(string $number): array
    {
        foreach ($_SESSION['country'] as $country) {
            if (preg_match('/^' . $country[2] . '/', $number)) {
                return [
                    'country' => $country[0],
                    'continent' => $country[1]
                ];
            }
        }
        return [];
    }

    /**
     * @param array $calls
     * @return array
     */
    public function compareContinentByPhoneWithContinentByIP(array $calls): array
    {
        foreach ($calls['calls'] as &$call) {
            foreach ($call['calls'] as &$item) {
                $ip = $item['customerIP'];
                $continent = $item['geoByPhone']['continent'];
                foreach ($calls['geo'] as $geo) {
                    if ($ip === $geo->ip && $continent === $geo->continentCode) {
                        $item['compareIpLocationWithPhoneLocation'] = 1;
                    }
                }
            }
        }
        return $calls;
    }

    /**
     * @param array $calls
     * @return array
     */
    public function addStatisticForEachCustomer(array &$calls): array
    {
        foreach ($calls['calls'] as &$call) {
            foreach ($call['calls'] as &$item) {
                $call['statistic']['allCustomerCalls'] ++;
                $call['statistic']['totalDurationCalls'] += (int) $item['durationInSeconds'];
            }
        }

        return $calls;
    }
}