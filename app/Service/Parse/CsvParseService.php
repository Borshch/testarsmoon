<?php
declare(strict_types=1);


namespace App\Service\Parse;


use App\App;
use stdClass;

class CsvParseService
{
    /**
     * @var array
     */
    protected array $result = ['geo' => []];

    /**
     * @return array
     */
    public function export(): array
    {
        if (($handle = fopen(   __DIR__ . '/../../../public/csv/upload.csv', "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $this->result['calls'][$data[0]]['calls'][] = [
                    'callDate' => $data[1],
                    'durationInSeconds' => $data[2],
                    'dialedPhoneNumber' => $data[3],
                    'customerIP' => $data[4],
                    'compareIpLocationWithPhoneLocation' => 0
                ];

                $this->result['calls'][$data[0]]['statistic'] = [];

                if (!$this->checkExistIp($data[4])) {
                    $newGeo = new stdClass();
                    $newGeo->ip = $data[4];
                    $this->result['geo'][] = $newGeo;
                }
            }
            fclose($handle);
        }

        return $this->result;
    }

    protected function checkExistIp(string $ip): bool
    {
        foreach ($this->result['geo'] as $geo) {
            if ($ip === $geo->ip) {
                return true;
            }
        }
        return false;
    }
}