<?php
declare(strict_types=1);

namespace App\Service\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;


class HttpService
{
    public function send($method = 'POST', $url = null, $body = [])
    {
        try {
            $client = new Client(['defaults' => [
                'verify' => false
            ]]);

            if ($method === 'POST') {
                $response = $client->request('POST', $url, [
                    'form_params' => $body
                ]);
            } else {
                $response = $client->request('GET', $url);
            }

            return $response;

        } catch (GuzzleException $e) {
            return $e->getMessage();
        }
    }
}