<?php
declare(strict_types=1);


namespace App\Service\Geo;


use App\App;
use App\Service\Http\HttpService;

class GeoService
{
    /**
     * @param array $customers
     * @return array
     */
    public function getContinent(array $customers): array
    {
        foreach ($customers['geo'] as $geo) {
            $continent = $this->send($geo->ip);
            $geo->continentName = $continent['continent_name'];
            $geo->continentCode = $continent['continent_code'];
        }

        return  $customers;
    }

    protected function send(string $ip)
    {
        $url = 'http://api.ipstack.com/' . $ip . '?access_key=d9f000dbc0237078dfb39bf8033d244c&format=1';
        $response = (new HttpService())->send('GET', $url)->getBody()->getContents();

        return json_decode($response, true);
    }
}